FROM java:latest
COPY ./target/ /home/target
WORKDIR /home/target
EXPOSE 80s
ENTRYPOINT ["java", "-jar", "*.jar", "--server.port=80"]
